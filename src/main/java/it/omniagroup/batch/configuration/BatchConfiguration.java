package it.omniagroup.batch.configuration;

import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaCursorItemReader;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import it.omniagroup.batch.listener.JobCompletionNotificationListener;
import it.omniagroup.batch.model.DematPdfa;
import it.omniagroup.batch.processor.DematPdfaProcessor;
import it.omniagroup.batch.reader.configuration.DematPdfaReader;
import it.omniagroup.batch.writer.DematPdfaWriter;

@Configuration
@EnableAutoConfiguration()
@EnableBatchProcessing
public class BatchConfiguration extends DefaultBatchConfigurer  {

	private static final Logger log = LoggerFactory.getLogger(DematPdfaProcessor.class);

	@Override
	public void setDataSource(DataSource dataSource) {
		super.setDataSource(dataSource);
	}

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;

	@Autowired
	private LocalContainerEntityManagerFactoryBean entityManagerFactoryBean;

	@Bean
	@Primary
	public JpaTransactionManager jpaTransactionManager() {

		final JpaTransactionManager tm = new JpaTransactionManager();
		return tm;
	}

	@Bean
	public JpaCursorItemReader<DematPdfa> reader() {

		JpaCursorItemReader<DematPdfa> jpaCursorReader;
		try {
			log.info("Inizio demat pdfa reader");

			jpaCursorReader = DematPdfaReader.build(entityManagerFactoryBean);

			log.info("Fine demat pdfa reader");
			
		} catch (Exception e) {
			log.error("ERRORE! Il Reader non è partito!");
			throw new IllegalArgumentException(e);
		}

		return jpaCursorReader;
	}

	@Bean
	public ItemProcessor<DematPdfa, DematPdfa> processor() {

		DematPdfaProcessor processor = null;
		
		try {
			log.info("Inizio demat pdfa processor");

			processor = new DematPdfaProcessor();

			log.info("Fine demat pdfa processor");
			
		} catch (Exception e) {
			log.error("ERRORE! Il processor non è partito!");
			throw new IllegalArgumentException(e);
		}

		return processor;
	}

	@Bean
	public JpaItemWriter<DematPdfa> writer() {

		JpaItemWriter<DematPdfa> jpaWriter = null;
		
		try {
			log.info("Inizio demat pdfa writer");

			jpaWriter = DematPdfaWriter.build(entityManagerFactoryBean);

			log.info("Fine demat pdfa writer");
			
		} catch (Exception e) {
			log.error("ERRORE! Il writer non è partito!");
			throw new IllegalArgumentException(e);
		}

		return jpaWriter;
	}

	@Bean
	public Step dematPdfConversionBatchStep() {

		TaskletStep step;
		try {
			log.info("Inizio demat pdfa Step");

			step = this.stepBuilderFactory.get("dematPdfConversionBatchStep")
					.transactionManager(jpaTransactionManager()).<DematPdfa, DematPdfa>chunk(1).reader(reader())
					.processor(processor()).writer(writer()).build();

			log.info("Fine demat pdfa Step");

		} catch (Exception e) {
			log.error("ERRORE! Lo Step non è partito!");
			throw new IllegalArgumentException(e);
		}

		return step;
	}

	@Bean
	public Job dematPdfConversionBatchJob(JobCompletionNotificationListener jobExecutionListener, Step step) {

		Job job = null;

		try {
			log.info("Inizio Demat job");

			job = jobBuilderFactory.get("job").listener(jobExecutionListener).start(step).build();

			log.info("Fine Demat Job");

		} catch (Exception e) {
			log.error("ERRORE! Il Job non è partito!");
			throw new IllegalArgumentException(e);
		}

		return job;
	}

}
