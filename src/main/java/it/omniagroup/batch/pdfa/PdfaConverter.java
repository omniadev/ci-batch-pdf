package it.omniagroup.batch.pdfa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.transform.TransformerException;

import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDMarkInfo;
import org.apache.pdfbox.pdmodel.documentinterchange.logicalstructure.PDStructureTreeRoot;
import org.apache.pdfbox.pdmodel.font.PDCIDFont;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDFontDescriptor;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDOutputIntent;
import org.apache.pdfbox.pdmodel.interactive.viewerpreferences.PDViewerPreferences;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.schema.AdobePDFSchema;
import org.apache.xmpbox.schema.DublinCoreSchema;
import org.apache.xmpbox.schema.PDFAIdentificationSchema;
import org.apache.xmpbox.schema.XMPBasicSchema;
import org.apache.xmpbox.type.BadFieldValueException;
import org.apache.xmpbox.xml.XmpSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import it.omniagroup.batch.processor.DematPdfaProcessor;

public class PdfaConverter {

	private static final Logger log = LoggerFactory.getLogger(DematPdfaProcessor.class);

	public byte[] convertPdfByteArrayToPdfa(byte[] pdfByteArray) throws IOException {

		// Convertitore pdf/A 1B

		PDDocument doc;
		ClassLoader classLoader = getClass().getClassLoader();

		try {
			log.info("Inizio caricamento PDF");
			doc = PDDocument.load(pdfByteArray);
			log.info("Fine caricamento PDF");
		} catch (IOException e) {
			log.error("ERRORE! Il PDF non può essere caricato e convertito in PDF/A!");
			throw new IllegalArgumentException(e);
		}

		removerTransparencyEntryFromPdfA(doc);

		// XMP Metadata
		setPdfaMetadata(doc);

		// ColorProfile
		setDocumentColorProfile(doc, classLoader);

		// set CIDFont
		extractCIDFontEmbeddedWithCIDSet(doc);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		doc.save(byteArrayOutputStream);
		doc.close();

		byte[] bytePdfaArray = byteArrayOutputStream.toByteArray();
		return bytePdfaArray;

	}

	public void removerTransparencyEntryFromPdfA(PDDocument doc) {

		try {
			log.info("Inizio rimozione trasparenza entry");

			PDPageTree pages = doc.getPages();

			for (PDPage page : pages) {

				page.getCOSObject();

				COSDictionary cosDictionarySource = page.getCOSObject();
				Set<Entry<COSName, COSBase>> entrySet = cosDictionarySource.entrySet();

				for (Entry<COSName, COSBase> entry : entrySet) {
					entry = entrySet.stream().findAny().get();
					COSName key = entry.getKey();

					if (key == COSName.GROUP) {
						cosDictionarySource.removeItem(COSName.GROUP);
					}
				}
			}

			log.info("Fine rimozione trasparenza entry");

		} catch (Exception e) {
			log.error("ERRORE! Le impostazioni non sono valide!");
			throw new IllegalArgumentException(e);
		}

	}

	public void extractCIDFontEmbeddedWithCIDSet(PDDocument doc) {

		try {

			log.info("Inizio settaggio dei Font");

			PDPage page = doc.getPage(0);

			for (COSName name : page.getResources().getFontNames()) {

				PDFont font = page.getResources().getFont(name);
				if (font instanceof PDType0Font) {

					PDType0Font type0font = (PDType0Font) font;
					PDCIDFont descendantFont = type0font.getDescendantFont();
					PDFontDescriptor fontDescriptor = descendantFont.getFontDescriptor();
					fontDescriptor.setCIDSet(new PDStream(doc));

				}
			}

			log.info("Fine settaggio dei Font");

		} catch (IOException e) {
			log.error("ERRORE! Le impostazioni non sono valide!");
			throw new IllegalArgumentException(e);
		}

	}

	public void setDocumentColorProfile(PDDocument doc, ClassLoader classLoader) {

		try {

			log.info("Inizio settaggio dei ColorProfile");
			InputStream colorProfile = classLoader.getResourceAsStream("assets/sRGB2014.icc");
			PDDocumentCatalog catalogue = doc.getDocumentCatalog();

			PDOutputIntent intent = new PDOutputIntent(doc, colorProfile);

			intent.setInfo("sRGB IEC61966-2.1");
			intent.setOutputCondition("sRGB IEC61966-2.1");
			intent.setOutputConditionIdentifier("sRGB IEC61966-2.1");
			intent.setRegistryName("http://www.color.org");

			catalogue.addOutputIntent(intent);

			PDViewerPreferences pdViewer = new PDViewerPreferences(doc.getPage(0).getCOSObject());
			pdViewer.setDisplayDocTitle(true);
			catalogue.setViewerPreferences(pdViewer);
			PDMarkInfo mark = new PDMarkInfo();
			PDStructureTreeRoot treeRoot = new PDStructureTreeRoot();
			catalogue.setMarkInfo(mark);
			catalogue.setStructureTreeRoot(treeRoot);
			catalogue.getMarkInfo().setMarked(true);

			log.info("Fine settaggio dei ColorProfile");

		} catch (Exception e) {
			log.error("ERRORE! Le impostazioni non sono valide!");
			throw new IllegalArgumentException(e);
		}

	}

	public void setPdfaMetadata(PDDocument doc) {

		try {

			log.info("Inizio settaggio dei Metadata");

			PDDocumentInformation documentInformation = doc.getDocumentInformation();
			PDDocumentCatalog catalogue = doc.getDocumentCatalog();
			documentInformation.setCreationDate(Calendar.getInstance());
			documentInformation.setModificationDate(Calendar.getInstance());

			// XMPMetadata
			XMPMetadata xmpMetadata = XMPMetadata.createXMPMetadata();

			// Dublin Core Schema
			DublinCoreSchema dc = xmpMetadata.createAndAddDublinCoreSchema();

			String docTitle = documentInformation.getTitle();
			if (docTitle != null) {
				dc.setTitle(docTitle);
			}

			String docCreator = documentInformation.getCreator();
			if (docCreator != null) {
				dc.addCreator(docCreator);
			}

			String docSubject = documentInformation.getSubject();
			if (docSubject != null) {
				dc.addSubject(docSubject);
			}

			String docProducer = documentInformation.getProducer();
			if (docSubject != null) {
				dc.addPublisher(docProducer);
			}

			// AdobePDFSchema
			AdobePDFSchema pdfSchema = xmpMetadata.createAndAddAdobePDFSchema();
			pdfSchema.setProducer(documentInformation.getProducer());

			// XMPBasicSchema
			XMPBasicSchema basicSchema = xmpMetadata.createAndAddXMPBasicSchema();
			basicSchema.setCreateDate(documentInformation.getCreationDate());
			basicSchema.setModifyDate(documentInformation.getModificationDate());
			basicSchema.setCreatorTool(documentInformation.getCreator());
			basicSchema.setMetadataDate(documentInformation.getCreationDate());

			XmpSerializer serializer = new XmpSerializer();

			PDMetadata metadata = new PDMetadata(doc);
			catalogue.setMetadata(metadata);

			PDFAIdentificationSchema identificationSchema = xmpMetadata.createAndAddPFAIdentificationSchema();
			identificationSchema.setConformance("B");
			identificationSchema.setPart(1);

			ByteArrayOutputStream xmpByteArrayOutputStream = convertXmpToByteArrayOutputStream(xmpMetadata, serializer);
			
			// da Xmp a Byte
			byte[] xmpByteArray = xmpByteArrayOutputStream.toByteArray();
			metadata.importXMPMetadata(xmpByteArray);

			log.info("Fine settaggio dei Metadata");

		} catch (BadFieldValueException e) {
			log.error("ERRORE! Le impostazioni non sono valide!");
			throw new IllegalArgumentException(e);
		} catch (IOException e) {
			log.error("ERRORE! Le impostazioni non sono valide!");
			throw new IllegalArgumentException(e);
		}
	}

	public ByteArrayOutputStream convertXmpToByteArrayOutputStream(XMPMetadata xmpMetadata, XmpSerializer serializer) {

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			serializer.serialize(xmpMetadata, byteArrayOutputStream, true);
			
		} catch (TransformerException e) {
			log.error("ERRORE! La conversione non è stata effettuata!");
			throw new IllegalArgumentException(e);
		}
		return byteArrayOutputStream;
		
	}

}
