package it.omniagroup.batch.pdfa;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.activation.DataSource;

import org.apache.pdfbox.preflight.PreflightDocument;
import org.apache.pdfbox.preflight.ValidationResult;
import org.apache.pdfbox.preflight.ValidationResult.ValidationError;
import org.apache.pdfbox.preflight.exception.ValidationException;
import org.apache.pdfbox.preflight.parser.PreflightParser;
import org.apache.pdfbox.preflight.utils.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PdfaValidator {

	private static final Logger log = LoggerFactory.getLogger(PdfaValidator.class);

	public boolean validatePDFADocument(byte[] pdfaByteArray) throws IOException {

		boolean result;

		try (PreflightDocument doc = createPreFlightDocument(pdfaByteArray)) {
			 
			ValidationResult validationResult = doc.getResult();
			result = validationResult.isValid();

			if (result) {
				log.info("PDFA validato!");
			} else {
				
				log.info("ERRORE! Il PDFA non è stato creato, perché non è validato correttamente!");
				
				List<ValidationError> errorsList = validationResult.getErrorsList();
				errorsList.forEach(h -> log.info(h.getDetails()));
			}

		} catch (ValidationException e) {
			result = false;
			log.error("Durante l'elaborazione incontra questo errore con l'id" + e.getMessage());
		}
		return result;
	}

	private PreflightDocument createPreFlightDocument(byte[] byteArray) throws IOException, ValidationException {

		InputStream inputstream = new ByteArrayInputStream(byteArray);
		DataSource data = new ByteArrayDataSource(inputstream);
		PreflightParser parser = new PreflightParser(data);
		parser.parse();
		PreflightDocument doc = parser.getPreflightDocument();
		doc.validate();
		return doc;

	}

}
