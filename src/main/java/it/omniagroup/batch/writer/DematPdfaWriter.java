package it.omniagroup.batch.writer;

import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.batch.item.database.builder.JpaItemWriterBuilder;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import it.omniagroup.batch.model.DematPdfa;

public class DematPdfaWriter extends JpaItemWriterBuilder<DematPdfa> {
	
	public static JpaItemWriter<DematPdfa> build(LocalContainerEntityManagerFactoryBean entityManagerFactory) {

		DematPdfaWriter writerFactory = new DematPdfaWriter();
		writerFactory.entityManagerFactory(entityManagerFactory.getObject());
		JpaItemWriter<DematPdfa> writer = writerFactory.build();

		return writer;
	}

}
