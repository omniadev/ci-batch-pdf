package it.omniagroup.batch.reader.configuration;

import java.util.Map;
import org.springframework.batch.item.database.JpaCursorItemReader;
import org.springframework.batch.item.database.builder.JpaCursorItemReaderBuilder;
import org.springframework.orm.jpa.AbstractEntityManagerFactoryBean;
import it.omniagroup.batch.model.DematPdfa;

public class DematPdfaReader extends JpaCursorItemReader<DematPdfa>{

	public static JpaCursorItemReader<DematPdfa> build(AbstractEntityManagerFactoryBean entityManagerFactory) 
	{
		return new JpaCursorItemReaderBuilder<DematPdfa>().name("jpa-cursor-item-reader")
				.entityManagerFactory(entityManagerFactory.getObject())
				.queryString("from DematPdfa where flgElaborato = :flgElaborato")
				.parameterValues(Map.of("flgElaborato", "N")).build();	
	}


}
