package it.omniagroup.batch.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

import it.omniagroup.batch.model.DematPdfa;
import it.omniagroup.batch.pdfa.PdfaConverter;
import it.omniagroup.batch.pdfa.PdfaValidator;


public class DematPdfaProcessor implements ItemProcessor<DematPdfa, DematPdfa> {

	private static final Logger log = LoggerFactory.getLogger(DematPdfaProcessor.class);

	PdfaConverter converter = new PdfaConverter();
	PdfaValidator validator = new PdfaValidator();

	@Override
	public DematPdfa process(DematPdfa item) throws Exception {
		
		log.info("Inizio del processo pdf con id: " + item.idUnivocoDocumento);

		byte[] pdfaDocument = converter.convertPdfByteArrayToPdfa(item.pdfFlat);

		Boolean result = validator.validatePDFADocument(pdfaDocument);

		if (result) {
			item.setPdfA(pdfaDocument);
			item.setFlgElaborato("S");
		} else {
			item.setFlgElaborato("E");
			log.info("Il documento con id: " + item.idUnivocoDocumento + " non passa la validazione");
		}

		log.info("Fine del processo pdf con id: " + item.idUnivocoDocumento);
		return item;
		
	}
}
