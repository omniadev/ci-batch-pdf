package it.omniagroup.batch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "DEMAT_PDFA")
public class DematPdfa {

	@Id
	public String idUnivocoDocumento;
	@Column(name = "PDF_FLAT")
	@Lob
	public byte[] pdfFlat;
	@Column(name = "FLG_ELABORATO")
	public String flgElaborato;
	@Lob
	@Column(name = "PDFA")
	public byte[] pdfA;


	public String getIdUnivocoDocumento() {
		return idUnivocoDocumento;
	}

	public void setIdUnivocoDocumento(String idUnivocoDocumento) {
		this.idUnivocoDocumento = idUnivocoDocumento;
	}

	public byte[] getPdfFlat() {
		return pdfFlat;
	}

	public void setPdfFlat(byte[] pdfFlat) {
		this.pdfFlat = pdfFlat;
	}

	public String getFlgElaborato() {
		return flgElaborato;
	}

	public void setFlgElaborato(String flgElaborato) {
		this.flgElaborato = flgElaborato;
	}

	public byte[] getPdfA() {
		return pdfA;
	}

	public void setPdfA(byte[] pdfA) {
		this.pdfA = pdfA;
	}

}
