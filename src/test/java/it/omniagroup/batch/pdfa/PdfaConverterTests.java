package it.omniagroup.batch.pdfa;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.TransformerException;

import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.xmpbox.XMPMetadata;
import org.apache.xmpbox.xml.XmpSerializer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PdfaConverterTests {

	private String originalPdfPath = "assets/pdfNormale.pdf";
	private String expectedPdfPath = "assets/pdfConvertito.pdf";

	@Test
	void convertPdfByteArrayToPdfa() throws Exception,IOException {

		byte[] originalPdf = returnStaticPdf(originalPdfPath);
		byte[] expectedPdf = returnStaticPdf(expectedPdfPath);
		
		PdfaConverter pdfaConverter = new PdfaConverter();		
		byte[] pdfaConverted = pdfaConverter.convertPdfByteArrayToPdfa(originalPdf);	
		
		PDFTextStripper pdfStripper = new PDFTextStripper();
		
		PDDocument pdfActual = PDDocument.load(pdfaConverted);
		String textOfPdfaActual = pdfStripper.getText(pdfActual);
		
		PDDocument pdfAExpected = PDDocument.load(expectedPdf);
		String textOfPdfaExpected = pdfStripper.getText(pdfAExpected);
		
		pdfActual.close();
		pdfAExpected.close();
		
		//se il contenuto testuale è lo stesso nei 2 pdfa
		assertEquals(textOfPdfaExpected,textOfPdfaActual); 

	}
	
	private byte[] returnStaticPdf(String pdfPath) throws FileNotFoundException, IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream file = classLoader.getResourceAsStream(pdfPath);
		byte[] bytes = IOUtils.toByteArray(file);
		return bytes;
	}
	
	@Test
	void convertXmpToByteArrayOutputStream() throws TransformerException  {
		
		PdfaConverter pdfaConverter = new PdfaConverter();
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		XmpSerializer serializer = new XmpSerializer();
		XMPMetadata xmpMetadata = XMPMetadata.createXMPMetadata();
		//conversione
		serializer.serialize(xmpMetadata , byteArrayOutputStream, true);
		
		ByteArrayOutputStream convertXmpToByte = pdfaConverter.convertXmpToByteArrayOutputStream(xmpMetadata, serializer);
		String xmpToByteOutStreamActual = convertXmpToByte.toString();
		String xmpToByteOutStreamExpected = byteArrayOutputStream.toString();
		
		assertEquals(xmpToByteOutStreamExpected,xmpToByteOutStreamActual); 
		
	}

}